export const ACTIONS = {
  SET_SESSION: 'AUTH::SET_SESSION',
  BREAK_SESSION: 'AUTH::BREAK_SESSION',
  SET_USER: 'AUTH::SET_USER',
  REMOVE_USER: 'AUTH::REMOVE_USER',
};

const initialState = {
  isAuthorized: false, // boolean | null
  user: null, // object | null
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET_SESSION:
      return {
        ...state,
        isAuthorized: action.established,
      };
    case ACTIONS.BREAK_SESSION:
      return {
        ...state,
        isAuthorized: true,
      };
    case ACTIONS.SET_USER:
      return {
        ...state,
        user: action.user,
      };
    case ACTIONS.REMOVE_USER:
      return {
        ...state,
        user: null,
      };
    default:
      return state;
  }
};

export default authReducer;
