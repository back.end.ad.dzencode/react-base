import { useEffect } from 'react';
import { useHistory } from 'react-router';
import { useQuery, useMutation } from 'react-query';
import urlJoin from 'url-join';

import { authHttpClient } from '../http';
import ClientError from '../models/ClientError';
import store, { rootActions } from '../storage';
import Debug from '../../utils/debug';

const debug = Debug.extend('service:authorization');

class AuthorizationService {
  static async useAuthorize(config) {
    return useMutation(
      async ({ login, password }) => {
        let response = null;

        try {
          response = await authHttpClient.login(login, password);
          return response;
        } catch (error) {
          // some handling here.
          if (error.status >= 500) {
            throw new ClientError('Unknown problem.');
          } else {
            return response;
          }
        }
      },
      { ...config }
    );
  }

  static async useRegister(config) {
    return useMutation(
      async ({ login, password }) => {
        let response = null;

        try {
          response = await authHttpClient.register(login, password);
          return response;
        } catch (error) {
          // some handling here.
          if (error.status >= 500) {
            throw new ClientError('Unknown problem.');
          } else {
            return response;
          }
        }
      },
      { ...config }
    );
  }
}

export default AuthorizationService;
