class ClientError {
  constructor(message, status) {
    this.message = message ?? 'Unknown problem.';
    this.status = status ?? 500;
  }

  get UserMessage() {
    return this.message;
  }
}

export default ClientError;
