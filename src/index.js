import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as StoreProvider } from 'react-redux';

import './utils/debug';

import App from './components/App/App';
import store from './api/storage';

import './assets/css/root.scss';
import './assets/css/base.scss';
import './assets/css/index.scss';
import './assets/css/typography.scss';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider store={store}>
      <App />
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
