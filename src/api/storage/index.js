import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { reducer as appReducer, appActions } from './app';
import { reducer as authReducer, authActions } from './auth';

const rootReducer = combineReducers({
  app: appReducer,
  auth: authReducer,
});

export const rootActions = {
  appActions,
  authActions,
};

export default createStore(rootReducer, applyMiddleware(thunk));
