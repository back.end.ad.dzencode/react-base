import { useCallback, useEffect, useState } from 'react';

/**
 * Detect direct click on element. Helpful with popups.
 * Should be used with `onMouseDown` event.
 *
 * @example
 * const close = () => {
 *  ...
 * };
 * const closeHandler = useDirectClickListener(close);
 *
 * ... on component ...
 * <Component
 *   onMouseDown={closeHandler}
 * />
 *
 * @param {Function} action - handler, that will be called on event.
 */
const useDirectClickListener = (action = () => {}) => {
  const [mouseUpHandler, setMouseUpHandler] = useState(null);

  const closeHandler = useCallback((firstEvent) => {
    if (firstEvent.currentTarget !== firstEvent.target) return;
    const el = firstEvent.currentTarget;
    const handler = (secondEvent) => {
      if (el === secondEvent.target) return action();
    };

    document.addEventListener('mouseup', handler, { once: true });
    setMouseUpHandler(() => handler);
  }, []);

  useEffect(() => {
    return () => {
      document.removeEventListener('mouseup', mouseUpHandler);
    };
  }, [mouseUpHandler]);

  return closeHandler;
};

export default useDirectClickListener;
