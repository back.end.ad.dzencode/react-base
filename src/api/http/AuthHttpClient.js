import urlJoin from 'url-join';

import HttpClient from './HttpClient';

class AuthHttpClient extends HttpClient {
  constructor(baseURL, { base, paths }) {
    super(urlJoin(baseURL + base));

    this.paths = {
      getUser: paths.getUser ?? '',
      register: paths.register ?? '',
      login: paths.login ?? '',
    };
  }

  async getUser() {
    return super.get(this.paths.getUser);
  }

  async register(login, password) {
    return super.post(this.paths.register, { login, password });
  }

  async login(login, password) {
    return super.post(this.paths.login, { login, password });
  }
}

export default AuthHttpClient;
