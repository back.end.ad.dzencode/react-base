import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'react-bootstrap';

import Header from './Header/Header';
import Footer from './Footer/Footer';

import styles from './layout.module.scss';

const Layout = (props) => {
  const { children } = props;

  return (
    <div className={styles['wrapper']}>
      <Header />
      <main className={styles['main']}>
        <Container>{children}</Container>
      </main>
      <Footer />
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
};

export default Layout;
