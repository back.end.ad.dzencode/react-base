/**
 * You can set any config related to the particular path.
 * E.g.
 * home: { path: '/', background: 'black', rollbackLink: '/login' },
 */
export default {
  home: { path: '/' },
  firstPage: { path: '/page-1' },
  signUp: { path: '/sign-up' },
  signIn: { path: '/sign-in' },
  account: { path: '/account' },
};
