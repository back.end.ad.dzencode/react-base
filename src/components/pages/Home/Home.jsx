import React from 'react';
import { Row, Col } from 'react-bootstrap';

import routeConfig from '../../../config/routes.config';

export const config = { path: routeConfig.home.path };

const Home = (props) => {
  return (
    <Row>
      <Col>Home</Col>
    </Row>
  );
};

export default Home;
