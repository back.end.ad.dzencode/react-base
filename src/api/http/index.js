import HttpClient from './HttpClient';
import AuthHttpClient from './AuthHttpClient';
import httpConfig from '../../config/http.config';

// try to establish url path to server from .env
const path = process.env.REACT_APP_NODE_HOST
  ? new URL(httpConfig.base, process.env.REACT_APP_NODE_HOST).href
  : httpConfig.base;

export const authHttpClient = new AuthHttpClient(path, httpConfig.models.user);

export default HttpClient;
