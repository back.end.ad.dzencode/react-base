import React, { useEffect } from 'react';

/**
 * Scroll page to top after `WrappedComponent` is mounted.
 * @param {React.Component} WrappedComponent
 */
const withScrollToTop = (WrappedComponent) => {
  const WithScrollToTop = (props) => {
    useEffect(() => {
      window.scrollTo(0, 0);
    }, []);

    return <WrappedComponent {...props} />;
  };

  return WithScrollToTop;
};

export default withScrollToTop;
