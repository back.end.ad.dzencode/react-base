export const ACTIONS = {
  SET_CONNECTION_STATE: 'APP::SET_CONNECTION_STATE',
};

const initialState = {
  connectionState: true,
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET_CONNECTION_STATE:
      return {
        ...state,
        connectionState: action.connectionState,
      };
    default:
      return state;
  }
};

export default appReducer;
