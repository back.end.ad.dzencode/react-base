import React from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import routeConfig from '../config/routes.config';

/**
 * Add authorization check on component
 * @param {boolean} [authorized] - should user be authorized
 * @param {string} [redirectLink] - link to witch user will be redirected if the condition are not satisfied
 */
const withAuthorization = (authorized = true, redirectLink = routeConfig.home.path) => {
  return (WrapperComponent) => {
    const WithAuthorization = (props) => {
      const { isAuthorized } = useSelector((state) => state.auth);
      if (authorized !== isAuthorized) return <Redirect to={redirectLink} />;
      return <WrapperComponent {...props} />;
    };

    return WithAuthorization;
  };
};

export default withAuthorization;
