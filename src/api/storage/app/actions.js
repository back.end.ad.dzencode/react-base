import { useDispatch } from 'react-redux';

import { ACTIONS } from './reducer';
import Debug from '../../../utils/debug';

const debug = Debug.extend('storage:actions:app');

export const setConnectionState = (connectionState = true) => {
  debug(`Get in setConnectionState action with connection state: %s`, connectionState);

  return async (dispatch) => {
    dispatch({
      type: ACTIONS.SET_CONNECTION_STATE,
      connectionState,
    });
    debug(`setConnectionState dispatched`);
  };
};

export const useAppActions = () => {
  const dispatch = useDispatch();
  return {
    setConnectionState: (connectionState) => dispatch(setConnectionState(connectionState)),
  };
};
