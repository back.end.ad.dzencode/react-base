import axios, { AxiosInstance, Method, AxiosRequestConfig, AxiosResponse } from 'axios';

import Debug from '../../utils/debug';

const debug = Debug.extend('connector');

class HttpClient {
  static authorizationToken = null;

  /**
   * @param {string} baseURL - base url of request e.g. '/user'
   * @param {AxiosRequestConfig} [config] - axios config
   * @param {(config: AxiosRequestConfig) => any} [extendedErrorHandler]
   */
  constructor(baseURL, config = {}, extendedErrorHandler) {
    this.baseURL = baseURL;
    this.config = config;
    this.extendedErrorHandler = extendedErrorHandler;
  }

  /**
   * Set auth token to the HttpClient class
   * @param {string} token
   */
  static authorize(token) {
    HttpClient.authorizationToken = token;
  }

  /**
   * Remove auth token from the HttpClient class
   */
  static unauthorize() {
    HttpClient.authorizationToken = null;
  }

  /**
   * Returns new HttpClient instance with auth token
   * @protected
   * @returns {HttpClient}
   */
  authorize() {
    const conn = new HttpClient(this.baseURL, this.config, this.extendedErrorHandler);
    conn.axios = axios.create({
      baseURL: this.baseURL,
      ...this.config,
      headers: {
        ...(this.config.headers || {}),
        Authorization: HttpClient.authorizationToken,
      },
    });

    return conn;
  }

  /**
   * Returns new HttpClient instance without auth token
   * @protected
   * @returns {HttpClient}
   */
  unauthorize() {
    const conn = new HttpClient(this.baseURL, this.config, this.extendedErrorHandler);
    const headers = { ...(this.config.headers || {}) };
    delete headers['Authorization'];
    conn.axios = axios.create({
      baseURL: this.baseURL,
      ...this.config,
      headers,
    });

    return conn;
  }

  /**
   * Returns axios instance
   * @private
   * @returns {AxiosInstance}
   */
  get Axios() {
    if (this.axios) return this.axios;
    if (HttpClient.authorizationToken) return this.authorize().axios;
    return axios.create({ ...this.config, baseURL: this.baseURL });
  }

  /**
   * Main request action
   * @param {string} path - url path
   * @param {Method} method - http method
   * @param {AxiosRequestConfig} [config] - additional axios config
   */
  async query(path, method, config = {}) {
    try {
      const response = await this.Axios.request({
        url: path,
        method: method,
        ...config,
      });

      return response.data.result;
    } catch (error) {
      if (error.response) return this.errorHandler(error.response, [path, method, config]);
      throw error;
    }
  }

  async get(path, params, config) {
    return this.query(path, 'GET', { ...config, params });
  }

  async post(path, data, config) {
    return this.query(path, 'POST', { ...config, data });
  }

  async put(path, data, config) {
    return this.query(path, 'PUT', { ...config, data });
  }

  async patch(path, data, config) {
    return this.query(path, 'PATCH', { ...config, data });
  }

  async delete(path, data, config) {
    return this.query(path, 'DELETE', { ...config, data });
  }

  /**
   * Default error handler. You can transform error here.
   * @param {AxiosResponse} response
   * @param {[path, method, config]} originPayload - original params from this.query method
   */
  async errorHandler(response, originPayload) {
    if (!response) {
      throw response.data.error;
    }

    switch (response.status) {
      case 401:
        debug(`Error Handler: trying to handle request error; code: 401, source: '%O'`, response);
        // ...
        throw response.data.error;
      default:
        break;
    }

    if (this.extendedErrorHandler) {
      debug(`Error Handler: trying to handle with Extended Error Handler`);
      return this.extendedErrorHandler(response);
    }

    throw response.data.error;
  }
}

export default HttpClient;
